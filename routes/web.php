<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/',[\App\Http\Controllers\TodoController::class,'showAll']);
Route::get('/show/{todo}',[\App\Http\Controllers\TodoController::class, 'showOne']);
Route::get('/create',[\App\Http\Controllers\TodoController::class,'create']);
Route::get('/edit/{todo}',[\App\Http\Controllers\TodoController::class,'edit']);
Route::get('/delete/{todo}',[\App\Http\Controllers\TodoController::class,'showBeforeDelete']);
Route::post('/create', [\App\Http\Controllers\TodoController::class, 'store']);
Route::put('/edit/{todo}', [\App\Http\Controllers\TodoController::class, 'save']);
Route::delete('/delete/{todo}', [\App\Http\Controllers\TodoController::class, 'delete']);

