<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Todo</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
            crossorigin="anonymous"></script>
</head>
<body>

<h1 class="m-3">TODO löschen</h1>

<form method="post" action="/delete/{{$todo->id}}">
    @csrf
    @method('DELETE')
    <div class="row m-3">
        <div class="col-md-6 col-sm-12">
            <label class="form-label" for="title">Titel</label>
            <input type="text" class="form-control" name="title" value="{{$todo->title}}">
        </div>

        <div class="col-md-6 col-sm-12">
            <label class="form-label" for="description">Beschreibung</label>
            <textarea class="form-control" name="description" rows="3">{{$todo->description}}</textarea>
        </div>
    </div>

    <div class="row m-3">
        <div class="col-md-4 col-sm-12">
            <label class="form-label" for="due">Fälligkeitsdatum</label>
            <input type="date" class="form-control" name="due" value="{{$todo->due}}">
        </div>

        <div class="col-md-4 col-sm-12">
            <label class="form-label" for="category">Kategorie</label>
            <input type="text" class="form-control" name="category" value="{{$todo->category}}">
        </div>

        <div class="col-md-4 col-sm-12">
            <label class="form-label" for="state">Status</label>
            <select class="form-select" name="state">
                <option hidden="" selected value="{{$todo->state}}">{{$todo->state}}</option>
                <option value="todo">Todo</option>
                <option value="in progress">In Bearbeitung</option>
                <option value="done">Erledigt</option>
            </select>
        </div>

    </div>

    <div class="row m-3">
        <div class="col-md-6 col-sm-12">
            <a href="/" type="submit" class="btn btn-info">Zurück</a>
            <button type="submit" class="btn btn-danger mx-3">Löschen</button>
        </div>
    </div>
</form>

</body>
</html>
