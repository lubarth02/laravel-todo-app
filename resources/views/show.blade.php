<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ToDO Details</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
            crossorigin="anonymous"></script>
</head>
<body>

<div class="m-3">
    <h1> ToDo Details</h1>

    <table class="table table-hover">
        <tbody>
        <tr>
            <td>Titel</td>
            <td>{{$todo->title}}</td>
        </tr>
        <tr>
            <td> Beschreibung</td>
            <td>{{$todo->description}}</td>
        </tr>
        <tr>
            <td> Fälligkeitsdatum</td>
            <td>{{$todo->due}}</td>
        </tr>
        <tr>
            <td> Kategorie </td>
            <td>{{$todo->category}}</td>
        </tr>
        <tr>
            <td>Status</td>
            <td>{{$todo->state}}</td>
        </tr>
        <tr>
            <td>Aktion</td>
            <td><a type="button" class="btn btn-warning" href="/edit/{{$todo->id}}">bearbeiten</a> <a type="button" class="btn btn-danger" href="/delete/{{$todo->id}}">löschen</a></td>
        </tr>
        </tbody>
    </table>
</div>

</body>
</html>
