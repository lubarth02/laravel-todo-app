<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Todo;
use Illuminate\Http\Request;
use Nette\Schema\ValidationException;

class TodoController extends Controller
{
    public function create()
    {
        return view('create');
    }

    public function store()
    {
        $data = request()->validate([
            'title' => 'required|string|min:3|max:50',
            'description' => 'required|string|min:10|max:255',
            'due' => 'required|date',
            'category' => 'required|string|min:3|max:50',
            'state' => 'required|string|min:3|max:50',
        ]);
        Todo::create($data);

        return redirect('/');
    }

    public function showAll()
    {
        $todos = Todo::all();
        return view('showAll')->with('todos',$todos);
    }

    public  function showOne(Todo $todo)
    {
        return view('show')->with('todo',$todo);
    }

    public function edit(Todo $todo) //Update Formular anzeigen
    {
        return view('edit')->with('todo',$todo);
    }

    public function save(Todo $todo) //Änderungen in Datenbank speichern
    {
        $data = request()->validate([
            'title' => 'required|string|min:3|max:50',
            'description' => 'required|string|min:10|max:255',
            'due' => 'required|date',
            'category' => 'required|string|min:3|max:50',
            'state' => 'required|string|min:3|max:50',
        ]);
        $todo->update($data);
        return redirect('/');
    }

    public function showBeforeDelete(Todo $todo)
    {
        return view('delete')->with('todo',$todo);
    }

    public function delete(Todo $todo)
    {
        $todo->delete();
        return redirect('/');
    }

}
